#!/bin/sh

PARANOIDAL="../paranoidal/mods"
MODS="$HOME/.factorio/mods"
MODLIST="$MODS/mod-list.json"
DISABLED_MODS="SurvivalGear TinyStart StoneWaterWell" # game become too easy

echo "Cleaning all installed mods..."

if [ -d $MODS ]; then
  rm -fr $MODS
fi

mkdir -p $MODS

for MODDIR in $PARANOIDAL ./mods
do
  echo "Installing mods from $MODDIR..."
  find $MODDIR -mindepth 1 -maxdepth 1 -type d -exec cp -rf {} $MODS \;
done

echo "Merging mod settings..."

factorio-mod-settings decode $PARANOIDAL/mod-settings.dat $MODS/mod-settings.json

for PATCH in ./cfg/*.json
do
  json-patch -o $MODS/mod-settings-new.json $PATCH $MODS/mod-settings.json
  rm -f $MODS/mod-settings.json
  mv $MODS/mod-settings-new.json $MODS/mod-settings.json
done

factorio-mod-settings encode $MODS/mod-settings.json $MODS/mod-settings.dat
rm -f $MODS/mod-settings.json

cat >$MODLIST <<EOF
{ "mods": [
    { "name": "base", "enabled": true }
EOF

for INFO in $MODS/*/info.json
do
  NAME=`cat "$INFO" | jq -r '.name'`
  echo "$DISABLED_MODS" | grep -q "$NAME"
  [ $? = "1" ] && VALUE=true || VALUE=false
  echo "  , { \"name\": \"$NAME\", \"enabled\": $VALUE }" >>$MODLIST
done

echo "]}" >>$MODLIST

echo "Done."

