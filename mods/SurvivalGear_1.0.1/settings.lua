data:extend({
  {
    type = "bool-setting",
    name = "survival-starting-armor",
    setting_type = "runtime-global",
    default_value = true,
    order = "a"
  },
  {
    type = "bool-setting",
    name = "survival-salvaged-machines",
    setting_type = "runtime-global",
    default_value = true,
    order = "b"
  },
  {
    type = "bool-setting",
    name = "survival-lazy-bastard",
    setting_type = "startup",
    default_value = false,
    order = "c"
  },
  {
    type = "bool-setting",
    name = "survival-hide-burner",
    setting_type = "startup",
    default_value = false,
    order = "d"
  }
})