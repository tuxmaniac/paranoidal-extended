local util = require("util")

if settings.global["survival-salvaged-machines"].value then
  script.on_init(function(event)
    if remote.interfaces["freeplay"] then
      local player_items = {
        ["pistol"] = 1,
        ["firearm-magazine"] = 15
      }
      local ship_items = {
        ["electric-mining-drill"] = 3,
        ["medium-electric-pole"] = 10,
        ["salvaged-assembling-machine"] = 2,
        ["salvaged-lab"] = 1,
        ["salvaged-generator"] = 1
      }
      if game.default_map_gen_settings.cliff_settings.richness > 0 then
        player_items["cliff-explosives"] = 10
      end
      if game.active_mods["Explosive Excavation"] then
        player_items["blasting-charge"] = 10
      end
      remote.call("freeplay", "set_created_items", player_items)
      remote.call("freeplay", "set_ship_items", ship_items)
    end
  end)
end

if settings.global["survival-starting-armor"].value then
  script.on_event(defines.events.on_cutscene_cancelled, function(event)
    util.insert_safe(game.players[event.player_index],{["survival-armor"] = 1})
    if game.players[event.player_index].character and game.players[event.player_index].character.grid and
      game.players[event.player_index].character.grid.prototype.name == "survival-equipment-grid" then
      local grid = game.players[event.player_index].character.grid
      local roboport = grid.put{name="personal-roboport-equipment"}
      if roboport then
        roboport.energy = roboport.max_energy
      end
      local battery = grid.put{name="battery-equipment"}
      if battery then
        battery.energy = battery.max_energy
      end
      grid.put{name="solar-panel-equipment"}
      grid.put{name="solar-panel-equipment"}
      grid.put{name="solar-panel-equipment"}
    else
      util.insert_safe(game.players[event.player_index],{["personal-roboport-equipment"] = 1,})
      util.insert_safe(game.players[event.player_index],{["battery-equipment"] = 1,})
      util.insert_safe(game.players[event.player_index],{["solar-panel-equipment"] = 3,})
    end
    util.insert_safe(game.players[event.player_index],{["construction-robot"] = 10})
  end)
end