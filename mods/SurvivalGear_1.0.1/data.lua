require("prototypes.animations")
require("prototypes.entities")
require("prototypes.items")

local function hide_recipe(recipe)
  if recipe then
    recipe.hide_from_player_crafting = true
    if recipe.normal then
      recipe.normal.hide_from_player_crafting = true
    end
    if recipe.expensive then
      recipe.expensive.hide_from_player_crafting = true
    end
  end
end

if settings.startup["survival-hide-burner"].value then
  hide_recipe(data.raw.recipe["burner-mining-drill"])
  hide_recipe(data.raw.recipe["burner-inserter"])
end
