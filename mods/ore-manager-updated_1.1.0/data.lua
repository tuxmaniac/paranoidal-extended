-- adamius.dev@gmail.com 2020 Licensed LGPL3
-- refer to the LICENSE.txt file for further details
--

require("util")

-- entity
local entity = util.table.deepcopy(data.raw["container"]["wooden-chest"])
entity.name = "ore-manager-chest"
entity.minable.result = "ore-manager-chest"
-- entity needs a real graphics

--Item
local item = util.table.deepcopy(data.raw["item"]["wooden-chest"])
item.name = "ore-manager-chest"
item.place_result = "ore-manager-chest"
item.tint = {r = 1, g = 1, b = 1}
--item.icon = "__ore-manager__/graphics/icon/ore-manager-chest.png"
item.icon_size = 64
item.subgroup = "terrain"
item.order = "e[ore-manager-chest]"
item.stackable = false
item.stack_size = 1

--Recipe
local recipe = util.table.deepcopy(data.raw["recipe"]["wooden-chest"])
recipe.name = "ore-manager-chest"
recipe.enabled = true
recipe.result = "ore-manager-chest"

data:extend({entity, item, recipe})
