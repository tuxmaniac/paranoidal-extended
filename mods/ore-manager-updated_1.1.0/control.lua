-- adamius.dev@gmail.com 2020 Licensed LGPL3
-- refer to the LICENSE.txt file for further details
--

function what_item_at(surface,position)
  for idx, ore in ipairs(surface.find_entities_filtered{type="resource",position=position,radius=1}) do
    return ore.name
  end
  return nil
end

function scan_for_ores( surface, here )
  local c = 0
  local nc = -1
  local radius = 32

  -- pick a target ore
  local item_name = what_item_at(surface,here)

  -- sanity check
  if item_name == nil then

    return { origin = {x = here.x, y = here.y}, name = "ore-manager-none", ores = {} }
  end

  -- search until it makes no difference
  while true do
    local found_ores = surface.find_entities_filtered{type="resource", name = item_name, position = here, radius = radius}
    c=nc
    nc = #found_ores

    if c == nc then

      local found={}
      for idx, ore in ipairs(found_ores) do
        found[#found+1] = {position = {x = ore.position.x-here.x, y = ore.position.y-here.y}, amount = ore.amount}
        ore.destroy()
      end
      return { origin = {x = here.x, y = here.y}, name = item_name, ores = found }
    end
    radius = radius * 2
  end
  return { origin = {x = here.x, y = here.y}, name = item_name, ores = {} }

end

function deploy_ore_patch(surface, position, patch)
  local deployed = 0
  if patch then
    for idx, ore in ipairs(patch.ores) do
      surface.create_entity{name = patch.name, position = {x = position.x + ore.position.x, y = position.y + ore.position.y }, amount = ore.amount}
      deployed = deployed + 1
    end
  end
end

function on_built_entity(event)
  local player = ""

  if event.player_index then
    player = game.players[event.player_index]
  else
    player = event.created_entity.last_user
  end

  if not player or player == "" then
    return
  end

  if event.created_entity.name == "ore-manager-chest" then
    if player.admin then

      -- deploy
      local item_name = what_item_at(event.created_entity.surface, event.created_entity.position)
      if item_name then

        global.queue = global.queue or {}
        global.queue[event.created_entity.unit_number] = scan_for_ores(event.created_entity.surface, event.created_entity.position)

      else

        global.queue = global.queue or {}

        if global.queue then

          for idx, patch in pairs(global.queue) do
            if patch then
              if #patch.ores then

                if patch.name ~= "ore-manager-none" then
                  global.queue[idx]=nil
                  if deploy_ore_patch(event.created_entity.surface, event.created_entity.position, patch) then
                    -- destroy the chest
                    event.created_entity.destroy()
                  end

                  return

                end
              end
            end

          end
        end
      end
    end
  end

end

function on_mined_entity(event)
  -- if the player or a bot mines a chest then we need to cancel its timer
  if event.entity then
    if event.entity.name == "ore-manager-chest" then


    end
  end

end

script.on_event({ defines.events.on_built_entity, defines.events.on_robot_built_entity }, on_built_entity)
script.on_event({ defines.events.on_player_mined_entity, defines.events.on_robot_mined_entity}, on_mined_entity)
