function exec_command(player)
    if not player.admin then
        player.print('You are not an admin. You may not use this mod. :(')
        return
    end

    local f, err, cmd

    if player.gui.left.some_luaconsole then
        global.cmd = player.gui.left.some_luaconsole.input.text
    end

    cmd = global.cmd or ""
    cmd = cmd:gsub('game%.player%.', 'game.players['..player.index..'].')

    f, err = loadstring(cmd)
    if not f then
        cmd = 'game.players['..player.index..'].print('..cmd..')'
        f, err = loadstring(cmd)
    end

    _, err = pcall(f)
    if err then
        player.print(cmd)
        player.print(err:sub(1, err:find('\n')))
    end
end


function toggleGui (player)
    if player.gui.left.some_luaconsole then
        global.cmd = player.gui.left.some_luaconsole.input.text
        player.gui.left.some_luaconsole.destroy()
    else
        frame = player.gui.left.add{type = 'frame',
            name = 'some_luaconsole',
            direction = 'vertical',
            caption = {'some_luaconsole.title'}
        }

        frame.add{type = 'label', caption = {'some_luaconsole.inputlabel'}}
        input = frame.add{type = 'text-box',
            name = 'input',
            style='some_luaconsole_input_textbox'
        }
        input.word_wrap = true
        input.text = global.cmd or ""

        horizontal_flow = frame.add{type='flow', direction='horizontal'}
        horizontal_flow.add{type = 'button',
            name = 'some_luaconsole_close',
            style='back_button',
            caption = {'some_luaconsole.close'},
            tooltip = {'some_luaconsole.close_tooltip'}
        }
        horizontal_flow.add{type = 'button',
            name = 'some_luaconsole_exec',
            style='confirm_button',
            caption = {'some_luaconsole.exec'},
            tooltip = {'some_luaconsole.exec_tooltip'}
        }

        if not player.admin then
            player.gui.left.some_luaconsole.input.text = 'You are not an admin. You may not use this mod. :('
            player.gui.left.some_luaconsole.input.enabled = false
        end
    end
end



script.on_event(defines.events.on_gui_click, function(event)
    if event.element.name == 'some_luaconsole_exec' then
        exec_command(game.players[event.player_index])
    elseif event.element.name == 'some_luaconsole_close' then
        toggleGui(game.players[event.player_index])
    end
end)


script.on_event('some_luaconsole_toggle', function(event)
    toggleGui(game.players[event.player_index])
end)
script.on_event('some_luaconsole_exec', function(event)
    exec_command(game.players[event.player_index])
end)
