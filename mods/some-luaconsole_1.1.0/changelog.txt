---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 2020-11-26

  Changed:
    - Version-Bump. No functional changes.

---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 2020-08-17

  Features:
    - Now remembers the input-script.
    - Last input can be executed without opening by using keybindings only.
    - Default keybinding to execute last input-script: CONTROL + ENTER.

  Changed:
    - Default keybinding to open: from SHIFT + RIGHTBRACKET to CONTROL + RIGHTBRACKET.

---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 2020-08-14

  Changed:
    - No functional changes.

---------------------------------------------------------------------------------------------------
Version: 0.18.1
Date: 2020-05-05

  Changed:
    - Moved git repository. No functional changes.

---------------------------------------------------------------------------------------------------
Version: 0.18.0
Date: 2020-01-24

  Changed:
    - Update to factorio 0.18.x.

---------------------------------------------------------------------------------------------------
Version: 0.17.3
Date: 2019-06-01

  Changed:
    - Code reformatting.

---------------------------------------------------------------------------------------------------
Version: 0.17.2
Date: 2019-03-17

  Changed:
    - Added check for admin status ... but this mod should not be used in multiplayer games anyway!

---------------------------------------------------------------------------------------------------
Version: 0.17.1
Date: 2019-03-17

  Changed:
    - Polished, gitified and published my transition helper mod.
