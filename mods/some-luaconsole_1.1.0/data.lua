data.raw["gui-style"].default["some_luaconsole_input_textbox"] = {
    type = "textbox_style",
    name = "some_console_input_textbox",
    parent = "textbox",
    minimal_width = 500,
    minimal_height = 60,
}


data:extend({
    {
        type = "custom-input",
        name = "some_luaconsole_toggle",
        key_sequence = "CONTROL + RIGHTBRACKET"
    },
    {
        type = "custom-input",
        name = "some_luaconsole_exec",
        key_sequence = "CONTROL + ENTER"
    },
})
