local noise = require("noise")
local util = require("mapGenUtil")

local tne = noise.to_noise_expression
local abs = noise.absolute_value
local max = noise.max
local min = noise.min


local function canal_scale_multiplier()
  return util.scale_slider( 1 / noise.var("control-setting:st-canals:frequency:multiplier"), 0.75)
end

local function moat_width_multiplier()
  -- This is the 'coverage' slider
  local s = noise.get_control_setting("st-canals").size_multiplier
  s = noise.less_than(s,1)/(-s) + (1-noise.less_than(s,1))*s
  s = noise.clamp((s+6) / 11,0,1)
  -- 0 < s < 1
  return 1-s
end

local function bridge_width_multiplier()
  -- This is the 'coverage' slider
  local s = noise.get_control_setting("st-terrastructure").size_multiplier
  s = noise.less_than(s,1)/(-s) + (1-noise.less_than(s,1))*s
  s = noise.clamp((s+6) / 11,0,1)
    -- 0 < s < 1
  return s
end

local function modulo(val, range)
    range = noise.absolute_value(range or 1)
    local quotient = val / range
    return (quotient - noise.floor(quotient)) * range -- noise.fmod(val, range)
end


local function diamond_aquastructure(x,y,tile,map)
  -- This is the 'Scale' slider
  local square_size = 200 * canal_scale_multiplier()

  local moat_width = util.lerp( 0.5, 1 - min(0.01, 2/square_size), moat_width_multiplier())

  local bridge_width = util.lerp(0, 0.2, bridge_width_multiplier())

  local center = util.square_center(x,y, square_size)

  local is_square_interior = noise.less_than(util.manhattan_distance_to_square_center(x,y,center.x,center.y), moat_width * square_size * 0.707106781186548)
  local is_bridge = noise.less_than(util.distance_to_square_axis(x,y,center.x,center.y), bridge_width * square_size)
  
  return 1 - (1 - is_bridge) * (1 - is_square_interior) * 2
end

local function grid_walls(x,y,tile,map)
  -- This is the 'Scale' slider
  local square_size = 160 * canal_scale_multiplier()

  local moat_width = util.lerp( 0.5, 1 - min(0.01, 2/square_size), moat_width_multiplier())
  local bridge_width = util.lerp(0, 0.125, bridge_width_multiplier())

  local center = util.square_center(x,y, square_size)

  local is_square_interior = noise.less_than(max(abs(x-center.x),abs(y-center.y)), moat_width * square_size / 2)
  local is_bridge = noise.less_than(util.distance_to_square_axis(x,y,center.x,center.y), bridge_width * square_size)
  
  return 1 - (1 - is_bridge) * (1 - is_square_interior) * 2
end

local function hex_moats(x,y,tile,map)

  -- This is the 'Scale' slider
  local hex_size = 120 * canal_scale_multiplier()
  local hex_side = 0.707106781186548*hex_size

  local moat_width = util.lerp( 0.5, 1 - min(0.01, 2/hex_size), moat_width_multiplier())
  local bridge_width = util.lerp(0, 0.075, bridge_width_multiplier())

  -- Find the fractional hex coord
  local f_hex = util.hex_coord_for_point(x, y, hex_size)
  local hex = util.hex_round( f_hex )
  local center = util.hex_center_for_qr(hex.q, hex.r, hex_size)

  local is_hex_interior = noise.less_than(util.cube_distance_to_hex_center(f_hex.q, f_hex.r, hex.q, hex.r), moat_width)
  local is_bridge = noise.less_than(util.distance_to_hex_cube_axis(f_hex.q, f_hex.r, hex.q, hex.r), bridge_width)
  
  return 1 - (1 - is_bridge) * (1 - is_hex_interior) * 2
end

local function band(v,separation,width)
  return noise.less_than(modulo(v, separation),width)
end

local function diamond_intersections_even(x,y,hex_side,hex_side_leg,width)
  sqrt2w = width * 1.414213562373095
  y_off = 0.5 * sqrt2w


  y = modulo(y + y_off, (hex_side + hex_side_leg) * 2) - y_off
  
  local draw = noise.less_than(y + y_off, hex_side_leg + sqrt2w)
  -- y = modulo(y + y_off, hex_side_leg + 2 * width) - y_off

  local bridge5 = band(x + y, hex_side_leg * 2,sqrt2w) 
  local bridge6 = band(x - y, hex_side_leg * 2,sqrt2w) 

  local bridge_diamonds = bridge5 * bridge6 

  return draw * bridge_diamonds
end

local function diamond_intersections_odd(x,y,hex_side,hex_side_leg,width)
  sqrt2w = width * 1.414213562373095

  y = y + hex_side
  y_off = 0.5 * sqrt2w + hex_side_leg
  y = modulo(y + y_off, (hex_side + hex_side_leg) * 2) - y_off
  
  local draw = noise.less_than(y + y_off, hex_side_leg + sqrt2w)

  local bridge5 = band(x + y, hex_side_leg * 2,sqrt2w) 
  local bridge6 = band(x - y, hex_side_leg * 2,sqrt2w) 

  local bridge_diamonds = bridge5 * bridge6 

  return draw * bridge_diamonds
end

local function diamond_intersections(x,y,hex_side,hex_side_leg,width)
  return diamond_intersections_odd(x, y, hex_side, hex_side_leg, width)
   + diamond_intersections_even(x, y, hex_side, hex_side_leg, width)
 end

local function hex_bridges(x,y,tile,map,hex_side,width, adjustment)

  adjustment = adjustment or 0
  width = width or 20
  sqrt2w = width * 1.414213562373095
  hex_side = hex_side or 100
  hex_side_leg = 0.707106781186548*hex_side

  local ys = util.slope_step(y, hex_side_leg, hex_side + hex_side_leg + adjustment)

  local pole_x_offset = -0.207106781186548 * width
  local bridge1 = band(x + pole_x_offset + ys, hex_side_leg * 2,width)
  local bridge2 = band(x + pole_x_offset - ys, hex_side_leg * 2,width)
  local bridge_poles = bridge1 * bridge2

  local bridge3 = band(x + ys, hex_side_leg * 2,sqrt2w) 
  local bridge4 = band(x - ys, hex_side_leg * 2,sqrt2w) 
  local bridge_diags = max(bridge3, bridge4) - bridge3 * bridge4

  local bridge_diamonds = diamond_intersections(x, y, hex_side + adjustment, hex_side_leg, width)

  return bridge_diamonds + bridge_diags + bridge_poles
end

local function hex_moats_display(x,y,tile,map)
  local scale = 100 * canal_scale_multiplier()
  local moat = 1.5 * scale * moat_width_multiplier()
  local y_offset = 1.207106781186548*scale - 0.353553390593274 * moat

  local is_moat = hex_bridges(x + 0.5*moat, y + y_offset, tile, map, scale, moat, -0.707106781186548 * moat )
  return is_moat
end

local function hex_moats_45(x,y,tile,map)
  local scale = 100 * canal_scale_multiplier()
  local moat = util.lerp(0.5*scale, 4, moat_width_multiplier())
  local bridge = scale * bridge_width_multiplier() / 4

  local y_offset = 1.207106781186548*scale - 0.353553390593274 * moat
  local adjustment = -0.292893218813452 * moat
  local is_moat =   hex_bridges(x + 0.707106781186548*moat                              , y + y_offset,tile,map,scale,moat,   adjustment )
  local is_bridge = hex_bridges(x + 0.707106781186548*bridge + 0.707106781186548 * scale, y + y_offset,tile,map,scale,bridge, adjustment )

  local height = 1.707106781186548 * scale + adjustment
  local stripe_offset = 0.5 * (scale + adjustment + bridge)
  local is_stripe_bridge = band(y + y_offset + stripe_offset, height, bridge)

  local is_land = max(is_bridge, is_stripe_bridge, 1 - is_moat)
  return is_land * 4 + (1-is_land) * -4
end


return {
  diamond_aquastructure = diamond_aquastructure,
  grid_walls = grid_walls,
  hex_moats = hex_moats,
  hex_moats_45 = hex_moats_45,
}

