local util = require("mapGenUtil")
local terraformer = require("mapGenNoise")
local aqua = require("aquastructure")
local noise = require("noise")
local mapGenDefault = require("mapGenDefault")

local tne = noise.to_noise_expression
local abs = noise.absolute_value
local max = noise.max
local min = noise.min

-- Sliders
data:extend({
  {
    type = "autoplace-control",
    name = "st-terrastructure",
    order = "z-a",
    category = "terrain",
    richness = true,
  },
  {
    type = "noise-expression",
    name = "control-setting:st-terrastructure:frequency:multiplier",
    expression = tne(1)
  },
  {
    type = "noise-expression",
    name = "control-setting:st-terrastructure:bias",
    expression = tne(0)
  },
  {
    type = "autoplace-control",
    name = "st-canals",
    order = "z-b",
    category = "terrain",
    richness = true,
  },
  {
    type = "noise-expression",
    name = "control-setting:st-canals:frequency:multiplier",
    expression = tne(1)
  },
  {
    type = "noise-expression",
    name = "control-setting:st-canals:bias",
    expression = tne(0)
  },
})

-- Aquastructure Pattern drop down
data:extend{
  {
    type = "noise-expression",
    name = "st-no-canals",
    order = "z-a",
    intended_property = "st-f-aquastructure",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return tne(1)
    end)
  },  
  {
    type = "noise-expression",
    name = "st-diamond",
    order = "z-b",
    intended_property = "st-f-aquastructure",
    expression = noise.define_noise_function(function(x, y, tile, map)
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return aqua.diamond_aquastructure(x,y,tile,map)
    end)
  },  
  {
    type = "noise-expression",
    name = "st-square-canals",
    order = "z-c",
    intended_property = "st-f-aquastructure",
    expression = noise.define_noise_function(function(x, y, tile, map)
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return aqua.grid_walls(x,y,tile,map)
    end)
  },  
  {
    type = "noise-expression",
    name = "st-hex-moats",
    order = "z-d",
    intended_property = "st-f-aquastructure",
    expression = noise.define_noise_function(function(x, y, tile, map)
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return aqua.hex_moats(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-hex-moats-45",
    order = "z-f",
    intended_property = "st-f-aquastructure",
    expression = noise.define_noise_function(function(x, y, tile, map)
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return aqua.hex_moats_45(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-hex-moats-45-flat",
    order = "z-f",
    intended_property = "st-f-aquastructure",
    expression = noise.define_noise_function(function(x, y, tile, map)
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return aqua.hex_moats_45(y,x,tile,map)
    end)
  },
}

-- Starting Lake drop down
data:extend{
  {
    type = "noise-expression",
    name = "st-no-lake",
    order = "z-a",
    intended_property = "st-starting-lake-bool",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return tne(0)
    end)
  },  
  {
    type = "noise-expression",
    name = "st-yes-lake",
    order = "z-b",
    intended_property = "st-starting-lake-bool",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return tne(1)
    end)
  }
}

-- Warp Mode Pattern drop down
data:extend{
  {
    type = "noise-expression",
    name = "st-no-warp",
    order = "z-a",
    intended_property = "st-warp-mode",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return tne(0)
    end)
  },  
  {
    type = "noise-expression",
    name = "st-warped",
    order = "z-b",
    intended_property = "st-warp-mode",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return tne(1)
    end)
  },
  {
    type = "noise-expression",
    name = "st-occasional-warp",
    order = "z-c",
    intended_property = "st-warp-mode",
    expression = noise.define_noise_function(function(x, y, tile, map)
      local warp = util.warp(x,y,map,3,{inscale = 1/60})
      warp = util.smoothstep(20,80,warp)
      return warp * 2
    end)
  },
  {
    type = "noise-expression",
    name = "st-maximum-warp",
    order = "z-d",
    intended_property = "st-warp-mode",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return util.warp(x,y,map,3) / 15
    end)
  },  
  {
    type = "noise-expression",
    name = "st-gone-plaid",
    order = "z-e",
    intended_property = "st-warp-mode",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return util.warp(x,y,map,3) / 5
    end)
  }
}

-- Elevation Basis Noise Vars
data:extend{
  {
    type = "noise-expression",
    name = "st-default-reimpl-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return mapGenDefault.default_world_elevation(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-default-mimic-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.default_mimic(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-oceans-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.ocean_world(x,y,tile,map)
    end)  
  },
  {
    type = "noise-expression",
    name = "st-island-web-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.island_web_world(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-hexy-reg-flat-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.hexy_world(x,y,tile,map,"hex-reg-flat")
    end)
  },
  {
    type = "noise-expression",
    name = "st-boxy-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.boxy_world(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-seafloor-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.seafloor_world(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-warp-ocean-map-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return terraformer.seafloor_world(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-warp-boxy-map-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return terraformer.boxy_world(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-warp-hexy-map-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      local warp_coords = util.warp_coordinates(x,y,tile,map)
      x = warp_coords.x
      y = warp_coords.y
      return terraformer.hexy_world(x,y,tile,map)
    end)
  },
  {
    type = "noise-expression",
    name = "st-river-maze-exp",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.river_maze_world(x,y,tile,map)
    end)
  }
}

-- Main Elevation Drop down
data:extend{
  {
    type = "noise-expression",
    name = "st-default-reimpl",
    intended_property = "elevation",
    order = "z-j",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-default-reimpl-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-default-mimic",
    intended_property = "elevation",
    order = "z-d",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-default-mimic-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-oceans",
    intended_property = "elevation",
    order = "z-a",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-oceans-exp"))
    end)  
  },
  {
    type = "noise-expression",
    name = "st-river-maze",
    intended_property = "elevation",
    order = "z-b1",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-river-maze-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-island-web",
    intended_property = "elevation",
    order = "z-b2",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-island-web-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-hexy-reg-flat",
    intended_property = "elevation",
    order = "z-e",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-hexy-reg-flat-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-boxy",
    intended_property = "elevation",
    order = "z-f",
    expression = noise.define_noise_function(function(x, y, tile, map)
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-boxy-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-seafloor",
    intended_property = "elevation",
    order = "z-c",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-seafloor-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-warp-boxy-map",
    intended_property = "elevation",
    order = "z-h",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-warp-boxy-map-exp"))
    end)
  },
  {
    type = "noise-expression",
    name = "st-warp-hexy-map",
    intended_property = "elevation",
    order = "z-g",
    expression = noise.define_noise_function(function(x, y, tile, map)      
      return terraformer.terraformed_world(x,y,tile,map,noise.var("st-warp-hexy-map-exp"))
    end)
  }
}